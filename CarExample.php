<?php

  abstract class Car {
    abstract public function applyBrake();
    public function increaseSpeed(){
      echo 'Increasing speed...<br>';
    }
    public function decreaseSpeed() {
      echo 'Decreasing speed...<br>';
    }
  }

    class SportsCar extends Car {
      //must implement applyBreak() method...
      function applyBrake() {
        echo "Braking...<br>";
      }
    }

  $car1 = new SportsCar();
  $car1->applyBrake();
  $car1->increaseSpeed();

  //Will throw error:
  //$car2 = new Car();
?>
