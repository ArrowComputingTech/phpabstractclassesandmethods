<?php

  require_once 'Database.php';

  class DB extends Database {
    public $connection;
    public $connectionActive = 'false';
    public $host;

    function setConnection($host) {
      echo "Connecting to " . $host . "<br>";
      $this->connectionActive = 'true';
      echo "Connected to " . $host . "<br>";
    }

    function getConnection() {
      if ($this->connectionActive) {
        echo "Connected.<br>";
      } else {
        echo "Not connected.<br>";
      }
    }

    function disconnect() {
      echo "Disconnecting...<br>";
      $this->connectionActive = 'false';
      echo "Disconnected.<br>";
    }
  }
  $db1 = new DB();
  $db1->connection = "example.com:5353";
  $db1->setConnection($db1->connection);
  $db1->getConnection();
  $db1->disconnect();

?>
