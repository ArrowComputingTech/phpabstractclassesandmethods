<?php

  abstract class Database {
    public $connection;
    public $connectionActive;

    abstract function setConnection($host);

    abstract function getConnection();

  }
